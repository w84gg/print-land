<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `type_from_customers`.
 */
class m180624_110427_drop_type_from_customers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('customers','type');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('customers', 'type', $this->integer());
    }
}
