<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customers`.
 */
class m180526_151934_create_customers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('customers', [
            'id' => $this->primaryKey(),
            'email' => $this->string(),
            'phone' => $this->string(),
            'name' => $this->string(),
            'type' => $this->integer(),
            'dt_created' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('customers');
    }
}
