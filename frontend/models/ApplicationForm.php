<?php
namespace frontend\models;

use yii\base\Model;
use backend\models\Customers;

class ApplicationForm extends Model
{
    public $name;
    public $email;
    public $phone;

    /**
     * {@inheritdoc}
     */

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Ваш E-mail адрес',
            'phone' => 'Ваш контактный номер телефона',
            'name' => 'Ваше имя',
            'dt_created' => 'Дата поступления',
        ];
    }

    public function rules()
    {
        return [
            ['name', 'trim'],
            ['name', 'required'],
            ['name', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\backend\models\Customers', 'message' => 'Данный адрес почты - уже используется. Пожалуйста введите корректный адрес.'],

            ['phone', 'trim'],
            ['phone', 'required'],
            ['phone', 'string', 'min' => 11],
        ];
    }

    public function application()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $customer = new Customers();
        $customer->name = $this->name;
        $customer->email = $this->email;
        $customer->phone = $this->phone;
        $customer->dt_created = date('Y-m-d h:i:s');

        return $customer->save() ? $customer : null;
    }
}


