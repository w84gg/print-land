<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use common\widgets\Alert;

$this->title = 'PrinterMap';

?>

<div class="text-center"><a href="/"><img src="images/logo.png" style="max-width: 100%;" /></a></div>

<header class="photo">
    <div class="container my-auto">
        <div class="row iit" style="float:left;">
            <div class="col-lg-12 text-left">
                <p style="font-size: 28px; text-shadow: 1px 1px 2px black, 0 0 1em black;">Онлайн-печать<br> для Вашей полиграфии<br> из любого места</p>
                <a target="_blank" class="btn btn-success btn-lg" href="http://test1.printermap.net">Перейти на демо-сайт</a>
            </div>
        </div>
    </div>
</header>
<section class="gallery" id="gallery">
    <div class="container my-auto">
        <div class="row">
            <div class="col-md-4 col-xs-12 text-center">
                <div class="service-box">
                    <img width="128" height="128" src="/images/icons/printer.png" alt="Онлайн-печать"
                         class="img-responsive mb-3">
                    <p class="service-desc mb-3">Дайте Вашим клиентам<br />больше возможностей</p>
                </div>
            </div>
            <div class="col-md-4 col-xs-12 text-center">
                <div class="service-box">
                    <img width="128" height="128" src="/images/icons/refresh.png" alt="Автообновления"
                         class="img-responsive mb-3">
                    <p class="service-desc mb-3">Ваши клиенты смогут оформить заказ<br />в любое время не выходя из дома</p>
                </div>
            </div>
            <div class="col-md-4 col-xs-12 text-center">
                <div class="service-box">
                    <img width="128" height="128" src="/images/icons/budget.png" alt="Гибкий тарифный план"
                         class="img-responsive mb-3">
                    <p class="service-desc mb-3">Без абонентской платы</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="services" id="services">
    <div class="container my-auto">
        <div class="row">

            <div class="col-md-6 col-sm-12 col-xs-12">
                <h1>Для владельцев бизнеса</h1>
                <p>Подключите услугу онлайн-печати фотографий и<br> начните получать заказы от пользователей<br> сервиса.</p>
                <a class="btn btn-info scroll-application" href="#application">Заполнить заявку</a>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="item_keys">
                    <div class="row">
                        <div class="col-md-2 col-xs-12">
                            <img class="img-responsive" src="/images/icons/24help.png" width="64" height="64"
                                 alt="Поддержка 24/7">
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <h3 class="land_title">Поддержка 24/7</h3>
                            <p>Ответим на ваши вопросы в любое время</p>
                        </div>

                        <div class="col-md-2 col-xs-12">
                            <img class="img-responsive" src="/images/icons/cloud.png" width="64" height="64"
                                 alt="Облачный сервис">
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <h3 class="land_title">Облачный сервис</h3>
                            <p>Наш сервис использует облачные возможности</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2 col-xs-12">
                            <img class="img-responsive" src="/images/icons/cp.png" width="64" height="64"
                                 alt="Панель управления">
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <h3 class="land_title">Панель управления</h3>
                            <p>Админ панель для вашего сайта бесплатно!</p>
                        </div>

                        <div class="col-md-2 col-xs-12">
                            <img class="img-responsive" src="/images/icons/settings.png" width="64" height="64"
                                 alt="Конфигурация приложения">
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <h3 class="land_title">Конфигурация приложения</h3>
                            <p>Настраивайте, меняйте дизайн под себя</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<section class="form" id="application">
    <div class="container my-auto">

        <h1>Заявка на подключение</h1>

        <? Pjax::begin(); ?>
        <?php
        $alert = Alert::widget();
        if ($alert) {
            $script = "scrollToAnchor('application')";
            $this->registerJs($script, yii\web\View::POS_READY);
        }
        echo $alert;
        ?>

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true])->input('name', ['placeholder' => 'Контактное имя'])->label(false) ?>

        <?= $form->field($model, 'phone')->textInput()->label(false)->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '+7 (999) 999-99-99',
        ])->input('phone', ['placeholder' => "Контактный телефон"]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true])->input('email', ['placeholder' => "E-mail адрес"])->label(false) ?>


        <div class="form-group">
            <?= Html::submitButton('Отправить заявку', ['class' => 'btn btn-apply']) ?>
        </div>

        <?php ActiveForm::end(); ?>
        <? Pjax::end(); ?>
    </div>
</section>