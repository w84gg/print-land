$( document ).ready(function() {
    $(".scroll-application").on("click", function() {
        scrollToAnchor("application");
    });
});

function scrollToAnchor(aid){
    var t = $("#" + aid);
    $('html,body').animate({scrollTop: t.offset().top},'slow');
}